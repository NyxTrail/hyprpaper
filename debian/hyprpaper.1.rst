:title: hyprpaper

NAME
====
hyprpaper - Wallpaper utility for Hyprland (and other wlroots-based compositors).

SYNOPSIS
========
**hyprpaper** [arg [...]]

OPTIONS
=======
**-h**, **--help**
   Show command usage.

**-c**, **--config**
   Specify config file to use

**--no-fractional**, **-n**
   Disable fractional scaling support

BUGS
====
Submit bug reports and request features online at:
  <*https://github.com/hyprwm/hyprpaper/issues*>

SEE ALSO
========
Sources at: <*https://github.com/hyprwm/hyprpaper*>

AUTHORS
=======
Created by Alan M Varghese <alan@digistorm.in> for Debian GNU/Linux packaging of hyprpaper
